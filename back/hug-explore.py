#!/usr/bin/env python
# coding: utf-8

# In[1]:


import explorers


# In[ ]:


"""Explorers api"""
import hug
def cors_support(response, *args, **kwargs):
    response.set_header('Access-Control-Allow-Origin', '*')

@hug.get('/', examples='city=singapore&filtertype=hotels', requires=cors_support)
def mercity(city: hug.types.text, filtertype: hug.types.text):
    """Returns city guide for city"""
    #explorers.merchantscity('singapore', 'hotels')
    return explorers.merchantscity(city, filtertype)


# In[ ]:


@hug.get('/flight', examples='cityfrom=sydney, citytoarrive=singapore, datefrom=04/03/2020, dateto=05/03/2020', requires=cors_support)
def ftrip(cityfrom: hug.types.text, citytoarrive: hug.types.text, datefrom: hug.types.text, dateto: hug.types.text):
    """Search for fight between two dates"""
    #explorers.merchantscity('singapore', 'hotels')
    return explorers.flyoneway(cityfrom, citytoarrive, datefrom, dateto)
#flightrip(locleave, locreturn, dateleave, datereturn)


# In[ ]:


@hug.post('/flightblog', examples='blogdir=/home/pi/artctrl, postname=a trip, cityfrom=sydney, citytoarrive=singapore, datefrom=04/03/2020, dateto=05/03/2020', requires=cors_support)
def ftrip(blogdir: hug.types.text, postname: hug.types.text, cityfrom: hug.types.text, citytoarrive: hug.types.text, datefrom: hug.types.text, dateto: hug.types.text):
    """Search for fight and create a blogpost with the search data"""
    #explorers.merchantscity('singapore', 'hotels')
    
    explorers.flyoneway(cityfrom, citytoarrive, datefrom, dateto)
    explorers.flyight2jsn(blogdir, postname, cityfrom, citytoarrive, datefrom, dateto)
    return('ok')
#flightrip(locleave, locreturn, dateleave, datereturn)


# In[1]:


#@hug.get('/returnflight', examples='locleave=singapore&locreturn=sydney&dateleave=01/01/2020, datereturn=02/02/2020', requires=cors_support)
#def retflight(locleave: hug.types.text, locreturn: hug.types.text, dateleave: hug.types.date, datereturn: hug.types.date):
#    """Search for fights"""
    #explorers.merchantscity('singapore', 'hotels')
#    return explorers.flightrip(locleave, locreturn, dateleave, datereturn)
#flightrip(locleave, locreturn, dateleave, datereturn)
#    flightdays('auckland', 'sydney')


# In[ ]:


#@hug.get('/f', examples='locleave=singapore&locreturn=sydney', requires=cors_support)
#def retflight(locleave: hug.types.text, locreturn: hug.types.text):
#    """Search for fights"""
    #explorers.merchantscity('singapore', 'hotels')
    #return explorers.flightrip(locleave, locreturn, dateleave, datereturn)
#flightrip(locleave, locreturn, dateleave, datereturn)
#    return(explorers.flightdays(locleave, locreturn))


# In[ ]:


@hug.get("/image.png", output=hug.output_format.png_image)
def create_image():
    image = Image.new("RGB", (100, 50))  # create the image
    ImageDraw.Draw(image).text((10, 10), "Hello World!", fill=(255, 0, 0))
    return image


# In[1]:


'''
'city': {'id': 'hong-kong_cn',
   'name': 'Hong Kong',
   'code': 'HKG',
   'slug': 'hong-kong-hong-kong-1',
   'subdivision': None,
   'autonomous_territory': None,
   'country': {'id': 'CN', 'name': 'China', 'slug': 'china', 'code': 'CN'},
   '''


# In[ ]:




